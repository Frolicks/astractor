﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class RandomImage : MonoBehaviour {
    public Sprite[] sprites; 

	// Use this for initialization
	void Start () {
        GetComponent<Image>().sprite = sprites[(int)Random.Range(0f, sprites.Length - 0.1f)]; 		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
