﻿using UnityEngine;
using System.Collections;

public class HazardSpawner : MonoBehaviour {
    public GameObject asteroid, blackhole;
    public EarthController gameStats; 

    private Vector2 earthPos;
    private float spawnRate;
    private string[] directions;
    private GameObject[] hazards; 
	// Use this for initialization
	void Start () {
        earthPos = GetComponent<RectTransform>().position;
        spawnRate = 0; 
        directions = new string[]{"UP", "DOWN", "LEFT", "RIGHT"};
        hazards = new GameObject[] { asteroid, blackhole };
	}

    // Update is called once per frame
    void Update () {
        if(spawnRate > 0 && !IsInvoking("spawnWave"))
        {
            Invoke("spawnWave", spawnRate); 
        }
    }
    public void waveSpawner(int days)
    {
        if (days >= 30 && days <= 39)
        {
            if (GameObject.FindGameObjectsWithTag("Burnable").Length == 1 && GameObject.FindGameObjectsWithTag("Blackhole").Length == 0)
            {
                GetComponent<GameController>().displayCongrats();
            }
        }
        if(days > 40)
        {
            for (int i = 0; i < (int)((days - 30) % 5); i++)
                spawnWave(); 
        }
        switch (days)
        {
            case 3:
                spawnWave(asteroid, 1, "RIGHT");
                break;
            case 5:
                spawnWave(asteroid, 3, "RIGHT");
                break;
            case 7:
                spawnWave(asteroid, 4, directions[(int)(Random.Range(0.1f, 3.9f))]);
                break;
            case 10:
                spawnWave(asteroid, 3, directions[(int)(Random.Range(0.1f, 3.9f))]);
                spawnWave(asteroid, 3, directions[(int)(Random.Range(0.1f, 3.9f))]);
                break;
            case 13:
                spawnWave(blackhole, 1, "RIGHT");
                spawnWave(asteroid, 3, "RIGHT");
                break;
            case 16:
                spawnWave(asteroid, 5, directions[(int)(Random.Range(0.1f, 3.9f))]);
                break;
            case 20:
                spawnWave(blackhole, 2, directions[(int)(Random.Range(0.1f, 3.9f))]);
                break;
            case 23:
                spawnWave(asteroid, 5, directions[(int)(Random.Range(0.1f, 3.9f))]);
                spawnWave(blackhole, 1, "RIGHT");
                break;
            case 25:
                spawnWave(asteroid, 5, directions[(int)(Random.Range(0.1f, 3.9f))]);
                spawnRate = 5;
                break;
            case 27:
                spawnRate = 0f;
                spawnWave(asteroid, 2, directions[(int)(Random.Range(0.1f, 3.9f))]);
                spawnWave(blackhole, 1, "RIGHT");
                spawnWave(asteroid, 3, directions[(int)(Random.Range(0.1f, 3.9f))]);
                break;
            case 40:
                GetComponent<GameController>().hideCongrats();
                spawnWave(asteroid, 1, directions[(int)(Random.Range(0.1f, 3.9f))]);
                spawnWave(asteroid, 1, directions[(int)(Random.Range(0.1f, 3.9f))]);
                spawnWave(asteroid, 1, directions[(int)(Random.Range(0.1f, 3.9f))]);
                break; 
            default:
                break; 
        }
    }

    private void spawnWave(GameObject hazard, int amount, string from) 
    {
        Vector2 center = Vector3.zero;
        float radius = 1;
        Vector2 initialForce = Vector2.zero; 
        switch(from)
        {
            case "UP":
                radius = Screen.width / 2; 
                center = new Vector2(earthPos.x, earthPos.y + Screen.height/2 + radius);
                initialForce = Vector2.down; 
                break;
            case "DOWN":
                radius = Screen.width / 2;
                center = new Vector2(earthPos.x, earthPos.y - Screen.height / 2 - radius);
                initialForce = Vector2.up; 
                break;
            case "LEFT":
                radius = Screen.height / 2;
                center = new Vector2(earthPos.x - Screen.width / 2 - radius, earthPos.y);
                initialForce = Vector2.right; 
                break;
            case "RIGHT":
                radius = Screen.height / 2;
                center = new Vector2(earthPos.x + Screen.width / 2 + radius, earthPos.y);
                initialForce = Vector2.left; 
                break; 
        }
        for(int i = 0; i < amount; i++)
        {
            GameObject spawned = spawnHazard(hazard, center + Random.insideUnitCircle * radius, Random.Range(0.7f, 1.7f));
            spawned.GetComponent<Rigidbody2D>().AddForce(initialForce * Random.Range(5f, 10f));
        } 
    }
    private void spawnWave()
    {
        Vector2 center = Vector3.zero;
        float radius = 1;
        Vector2 initialForce = Vector2.zero;
        string from = directions[(int)(Random.Range(0.1f, 3.9f))];
        switch (from)
        {
            case "UP":
                radius = Screen.width / 2;
                center = new Vector2(earthPos.x, earthPos.y + Screen.height / 2 + radius);
                initialForce = Vector2.down;
                break;
            case "DOWN":
                radius = Screen.width / 2;
                center = new Vector2(earthPos.x, earthPos.y - Screen.height / 2 - radius);
                initialForce = Vector2.up;
                break;
            case "LEFT":
                radius = Screen.height / 2;
                center = new Vector2(earthPos.x - Screen.width / 2 - radius, earthPos.y);
                initialForce = Vector2.right;
                break;
            case "RIGHT":
                radius = Screen.height / 2;
                center = new Vector2(earthPos.x + Screen.width / 2 + radius, earthPos.y);
                initialForce = Vector2.left;
                break;
        }
     
        for (int i = 0; i < (int)Random.Range(0,4); i++)
        {
            GameObject spawned = spawnHazard(hazards[(int)Random.Range(0,1.9f)], center + Random.insideUnitCircle * radius, Random.Range(0.5f, 2f));
            spawned.GetComponent<Rigidbody2D>().AddForce(initialForce * Random.Range(5f, 10f));
        }

        if(spawnRate > 0f)
        {
            Invoke("spawnWave", spawnRate);
        }

    }

    private GameObject spawnHazard(GameObject h, Vector2 pos, float size)
    {
        GameObject spawned = Instantiate(h, Vector3.one, Quaternion.identity) as GameObject;
        spawned.transform.SetParent(transform, false);

        RectTransform spawnedRT = spawned.GetComponent<RectTransform>();
        spawnedRT.localScale *= size;
        spawnedRT.position = pos;

        return spawned; 

        //spawnedRT.position = earth + Random.insideUnitCircle.normalized * (Screen.width/2 + 10f); //Random.Range(800f, 1500f); TARGETED SPAWNING  
    }


}
