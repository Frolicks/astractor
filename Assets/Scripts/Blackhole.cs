﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Blackhole : MonoBehaviour {
    public int absorbs;

    private Image img;
    private void Start()
    {
        img = GetComponent<Image>();
    }
    private void Update()
    {
        if(absorbs > transform.localScale.x)
        {
            disappear();  
        }
        if (img.color.a < 0.2f)
        {
            Destroy(gameObject);
        }
    }
    private void disappear()
    {
        transform.localScale *= 1.1f;
        img.CrossFadeAlpha(0f, 0.5f, false);
        Invoke("killMySelf", 0.5f); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Blackhole"))
        {
            Rigidbody2D rb = GetComponent<Rigidbody2D>(); 
            rb.AddForce((collision.GetComponent<Rigidbody2D>().position - rb.position).normalized * 0.2f, ForceMode2D.Impulse);
            disappear(); 
        }
    }

    private void killMySelf()
    {
        Destroy(gameObject); 
    }
}
