﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverButton : MonoBehaviour {
    public GameObject showedImage; 

    public void onEnter()
    {
        showedImage.SetActive(true);
    }

    public void onExit()
    {
        showedImage.SetActive(false); 
    }
}
