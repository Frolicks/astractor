﻿using UnityEngine;
using System.Collections;

public class AttractTo : MonoBehaviour {

    private Rigidbody2D rb;
    private bool aggroed;
    private RectTransform rt; 
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        rt = GetComponent<RectTransform>();
        if(GetComponentInParent<Canvas>().pixelRect.Contains(rt.position))
        {
            aggroed = true;
        }

        if (aggroed)
        {
            GameObject[] pulledBy = GameObject.FindGameObjectsWithTag("GravitationalPull"); 

            foreach(GameObject ob in pulledBy)
            {
                if (ob.transform.localScale.x > 0.5f) 
                    rb.AddForce((ob.GetComponent<Rigidbody2D>().position - rb.position).normalized * ob.GetComponent<Properties>().pullForce * Time.deltaTime);
            }
        }

	}
}
