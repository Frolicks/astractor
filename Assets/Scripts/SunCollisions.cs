﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; 

public class SunCollisions : MonoBehaviour {
    private AudioSource[] sounds;
    private void Start()
    {
        sounds = GetComponents<AudioSource>(); 
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        sounds = GetComponents<AudioSource>();
        if (Time.time > 0.1f)
        {
            if (collision.gameObject.CompareTag("Burnable"))
            {
                Destroy(collision.gameObject);
                GetComponent<SunAnimator>().happyFace(); 
                sounds[0].Play();
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("HoverButton"))
        {
            collision.gameObject.GetComponent<HoverButton>().onEnter(); 
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("HoverButton"))
        {
            collision.gameObject.GetComponent<HoverButton>().onExit();
        }
    }

}
