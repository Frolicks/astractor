﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInAndOut : FadeIn {
    public float stayDuration; 

	// Use this for initialization
	void Start () {

	}

    // Update is called once per frame

    private IEnumerator fadeInAndOut()
    {
        fadeIn();
        yieldReturn
    }
    public void fadeOut()
    {
        images = transform.GetComponentsInChildren<Image>();
        for (int i = 0; i < images.Length; i++)
        {
            images[i].CrossFadeAlpha(0f, 0f, false);
            images[i].CrossFadeAlpha(1f, fadeDuration, false);
        }
    }
}
