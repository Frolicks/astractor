﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {
    public float fadeDuration; 
    private Image[] images; 

	// Use this for initialization
	void Start () {
        fadeIn(); 
	}

    public void fadeIn()
    {
        images = transform.GetComponentsInChildren<Image>();
        for (int i = 0; i < images.Length; i++)
        {
            images[i].CrossFadeAlpha(0f, 0f, false);
            images[i].CrossFadeAlpha(1f, fadeDuration, false);
        }
    }
}
