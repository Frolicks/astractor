﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class SunAnimator : MonoBehaviour {

    public Sprite[] rays, eyes, mouths;

    public Image ray, eye, mouth; 

    private float happyTimer;

	// Use this for initialization
	void Start () {
        StartCoroutine(changeRay());
        happyTimer = 0f; 
	}
	
	// Update is called once per frame
	void Update () {
        if (happyTimer < Time.time)
        {
            if (GameObject.Find("Earth(Clone)"))
            {
                eye.sprite = eyes[0];
                mouth.sprite = mouths[0];
            }
            else
            {
                eye.sprite = eyes[1];
                mouth.sprite = mouths[1];
            }
        }

	}

    public void happyFace()
    {
        eye.sprite = eyes[2];
        mouth.sprite = mouths[2];
        happyTimer = Time.time + 0.5f;  
    }

    private IEnumerator changeRay()
    {
        while (true)
        {
            if (ray.sprite.Equals(rays[0]))
            {
                ray.sprite = rays[1];
            } else
            {
                ray.sprite = rays[0];
            }
            yield return new WaitForSeconds(0.7f);
            

        }
    }
}
