﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement; 

public class GameController : MonoBehaviour {
    public GameObject earth, sun;

    public GameObject title, startPrompt, congrats, howToPlay, credits;

    private bool eSpawnable; 

    private AudioSource[] sounds;

    private void Start()
    {
        eSpawnable = true;
        sounds = GetComponents<AudioSource>(); 
        StartCoroutine(checkBodyPresence()); 
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
        }
    }
    public void startGame()
    {
        title.SetActive(false);
        Destroy(startPrompt);
        Destroy(howToPlay);
        Destroy(credits); 

        if(!sounds[0].isPlaying)
            sounds[0].Play(); 
    }
    
    public void pause()
    {
        title.SetActive(true);
        Time.timeScale = 0f; 
    }

    public void unPause()
    {
        if (GameObject.Find("Start") == null)
            title.SetActive(false);
        Time.timeScale = 1f; 
    }

    public void hideMenu()
    {
        eSpawnable = false; 
        title.SetActive(false);
        startPrompt.SetActive(false);
        GameObject currentEarth = GameObject.Find("Earth(Clone)");
        if (currentEarth != null)
        {
            currentEarth.SetActive(false); 
        }
    }
    
    public void showMenu()
    {
        eSpawnable = true;
        title.SetActive(true);
        startPrompt.SetActive(true);
        GameObject currentEarth = GameObject.Find("Earth(Clone)");
        if (currentEarth != null)
        {
            currentEarth.SetActive(true);
        }
    }

    public void displayCongrats()
    {
        if(!congrats.activeSelf)
        {
            congrats.SetActive(true);
            congrats.GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
            congrats.GetComponent<Image>().CrossFadeAlpha(1f, 2f, false);
        }

    }

    public void hideCongrats()
    {
        congrats.SetActive(false);
    }
    private IEnumerator checkBodyPresence()
    {
        while (true)
        {
            if (eSpawnable && !GameObject.Find("Earth(Clone)"))
            {
                GameObject sEarth = Instantiate(earth, transform) as GameObject;
                if (!GameObject.Find("Sun(Clone)"))
                {
                    GameObject sSun = Instantiate(sun, transform) as GameObject;
                    sEarth.GetComponent<EarthController>().sun = sSun;
                }
                sEarth.transform.SetSiblingIndex(transform.childCount - 5);
                if (congrats.activeSelf)
                {
                    hideCongrats(); 
                }
            }
            yield return new WaitForSeconds(2f); 
        }

    }

    private IEnumerator FadeText(Text img, bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 1; i >= 0; i -= 0.25f * Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 1; i += Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(1, 1, 1, i);
                yield return null;
            }
        }
    }
}
 



