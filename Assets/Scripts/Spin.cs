﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

    public float speed;

    private RectTransform rt; 

	// Use this for initialization
	void Start () {
        rt = GetComponent<RectTransform>(); 
	}
	
	// Update is called once per frame
	void Update () {
        rt.rotation = Quaternion.Euler(0, 0, rt.rotation.eulerAngles.z + speed * Time.deltaTime); 
	}
}
