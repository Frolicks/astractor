﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EatableByBH : MonoBehaviour {

    private GameObject eater;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>(); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Blackhole"))
        {
            eater = collision.gameObject;
            eater.GetComponent<Blackhole>().absorbs++;
        }
    }

    private void Update()
    {
        if(eater != null)
        {
            rb.AddForce((eater.GetComponent<Rigidbody2D>().position - rb.position).normalized * 10f);
            transform.localScale *= 0.7f;
        }
        if (transform.localScale.x < 0.1f)
        {
            if(gameObject.name == "Earth(Clone)")
            {
                GetComponent<EarthController>().die(); 
            }
            if (gameObject.CompareTag("Burnable") || gameObject.name == "Sun(Clone)") 
            {
                Destroy(gameObject);
            }
            
        }
    }
}
