﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class MouseMove : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public bool followMouse;

    private void Update()
    {
        if (followMouse)
        {
            Vector2 mousePos = Input.mousePosition; 
            transform.position = new Vector3(mousePos.x, mousePos.y, 0f);
            if (!(GetComponentInParent<Canvas>().pixelRect.Contains(mousePos)))
            {
                GetComponentInParent<GameController>().pause(); 
            } else
            {
                GetComponentInParent<GameController>().unPause();
            } 
        }
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 pointerPos = eventData.position;
        transform.position = new Vector3(pointerPos.x, pointerPos.y, 0); 

    }

    public void OnEndDrag(PointerEventData eventData)
    {
    }

    static public T FindInParents<T>(GameObject go) where T : Component
    {
        if (go == null) return null;
        var comp = go.GetComponent<T>();

        if (comp != null)
            return comp;

        var t = go.transform.parent;
        while (t != null && comp == null)
        {
            comp = t.gameObject.GetComponent<T>();
            t = t.parent;
        }
        return comp;
    }

    private void OnDestroy()
    {
        Time.timeScale = 1f; 
    }
}
