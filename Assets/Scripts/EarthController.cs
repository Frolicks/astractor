﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EarthController : MonoBehaviour {
    public float heatChangeRate;
    public int days;
    public GameObject sun;
    public Sprite[] forms; 

    private Text daysDisplay; 
    private Vector2 sunPos, earthPos;
    private float distToSun, sunAngle, dayProgress, eTemp, angleRecordTimer;
    private Canvas canvas;
    private bool aWorld;
    private Image img; 

    private AudioSource[] sounds; 

    private void Start()
    {s
        img = GetComponent<Image>();

        days = 0; 
        eTemp = 30;  
        canvas = transform.GetComponentInParent<Canvas>(); 
        earthPos = GetComponent<RectTransform>().position;
        daysDisplay = GameObject.Find("Days").GetComponent<Text>();
        sun = GameObject.Find("Sun(Clone)");

        angleRecordTimer = Time.time + 0.1f; 

        sounds = GetComponents<AudioSource>(); 
    }

    private void Update()
    {
        if(sun != null)
        {
            sunPos = sun.GetComponent<RectTransform>().position; 
        }
        handleHeat();
        handleDays();

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        die(); 
    }


    private void handleHeat()
    {
        distToSun = Vector2.Distance(earthPos, sunPos);
        float heatPortion = 5;
        float coldPortion = 2f;
        if (distToSun > Screen.height / coldPortion || sun == null)
        {
            eTemp += heatChangeRate * Mathf.Lerp(1, 3f, (distToSun - Screen.height/coldPortion) / (Screen.height / coldPortion)) * Time.deltaTime * -1;
        }
        else if (distToSun < Screen.height / heatPortion)
        {
            eTemp += heatChangeRate * Mathf.Lerp(0.7f, 3f, (Screen.height/heatPortion - distToSun) / (Screen.height/heatPortion)) *  Time.deltaTime;
        }


        if (eTemp < 20 || eTemp > 40)
        {
            die(); 
        }

        if (eTemp < 27)
        {
            img.sprite = forms[3];
            shakeAtPos(earthPos);
            if (!sounds[1].isPlaying && eTemp < 25)
            {
                sounds[1].Play();
            }
 
        }
        else if (eTemp > 33 )
        {
            img.sprite = forms[2]; 
            if (!sounds[0].isPlaying) 
                sounds[0].Play();
        }
        else
        {
            if(days > 0)
                img.sprite = forms[1]; 
            sounds[0].Stop();
            sounds[1].Stop();
        }
    }
    private void handleDays()
    {
        if (Time.time < 1)
        {
            sunAngle = getSunAngle(); // initialized only when position is set
        }
        float newSunAngle = getSunAngle();
        float deltaAngle = newSunAngle - sunAngle; 
        if(Time.time > angleRecordTimer) {
            if (Mathf.Abs(deltaAngle) < 300) // didn't cross 0 axis 
                dayProgress += newSunAngle - sunAngle;
            else if (Time.time > 0.1f)
            {
                if (deltaAngle < 0)  // going counter-clockwise
                {
                    dayProgress += (360 + deltaAngle);
                }
                else                          // going clockwise
                {
                    dayProgress += (deltaAngle - 360);
                }
            }
        }

        if(Mathf.Abs(dayProgress) > 360)
        {
            days++;
            GetComponentInParent<HazardSpawner>().waveSpawner(days); 
            dayProgress = dayProgress % 360;

            sounds[2].time = 0.4f;
            sounds[2].Play(); 

            if(days == 1)
            {
                img.sprite = forms[1];
                daysDisplay.text = "1 Day a World";
                canvas.GetComponent<GameController>().startGame(); 
            }
            if(days == 30)
            {
                canvas.GetComponent<GameController>().displayCongrats(); 
            }
        }
        sunAngle = newSunAngle;

        if(days > 1)
            daysDisplay.text = days + " Days a World"; 
    }
    public void die()
    {
        GameObject.Find("Days").GetComponent<Text>().text = ""; 
        Destroy(gameObject);
    }
    private float aTan2PosDeg(float y, float x)
    {
        float deg = Mathf.Atan2(x, y) * 180f/Mathf.PI; 
        if (deg < 0)
        {
            deg += 360; 
        }
        return deg; 
    }
    private float getSunAngle()
    {
        return aTan2PosDeg(sunPos.x - earthPos.x, sunPos.y - earthPos.y);
    }
    void shakeAtPos(Vector2 center)
    {
        GetComponent<RectTransform>().position = Random.insideUnitCircle * 3f + center; 
    }
}
